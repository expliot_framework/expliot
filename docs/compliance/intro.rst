IoT Compliance
===============

AS IoT products take over the traditional and new ecosystem, it not only becomes
important to secure them from attacks but also have regulations and compliance
to make sure vendors adhere to secure standards for design, development and use.

Governments and non-profit organizations around the world are coming up with their
own IoT standards and guidelines to ensure security and privacy in the products.

It is needless to say that the industry will need tools and automation for
compliance. EXPLIoT Framework aims to ease IoT compliance verification and
we plan to add compliance modules and plugins over time to help the community.

Compliance Modules
------------------

.. toctree::
    :maxdepth: 1
    :glob:

    **