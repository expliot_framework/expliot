"""Wrapper for the i2c communication."""

from expliot.core.interfaces.ftdi import (
    I2cController,
    I2cEepromManager,
    I2cNackError,
)
